﻿// <copyright file="ViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ArcherMan_V0CMMQ.Elements
{
    using System.Media;
    using System.Windows;
    using System.Windows.Input;
    using ArcherMan_V0CMMQ.Elements.Arrows;
    using ArcherMan_V0CMMQ.View;

    /// <summary>
    /// Global variables are stored here.
    /// </summary>
    public sealed class ViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModel"/> class. Sets the important variables to default.
        /// </summary>
        /// <param name="game">The current <see cref="ArcherGameVisual"/> that represents the game.</param>
        public ViewModel(ArcherGameVisual game)
        {
            this.Game = game;

            Left = false;
            Right = false;
            Up = 0;
            Gravity = true;

            Dodge = false;
            ShootArrow = false;
            ArrowBack = false;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="Player"/> needs to move left.
        /// </summary>
        public static bool Left { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="Player"/> needs to move right.
        /// </summary>
        public static bool Right { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the graity is on.
        /// </summary>
        public static bool Gravity { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="Player"/> needs to jump.
        /// </summary>
        public static byte Up { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="Player"/> needs to dodge.
        /// </summary>
        public static bool Dodge { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether an <see cref="Arrow"/> is need to be shot.
        /// </summary>
        public static bool ShootArrow { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether an <see cref="Arrow"/> need to be called back.
        /// </summary>
        public static bool ArrowBack { get; set; }

        /// <summary>
        /// Gets or sets the shooting direction of the current <see cref="Arrow"/>.
        /// </summary>
        public static Direction ShootingDirection { get; set; }

        /// <summary>
        /// Gets or sets the selected <see cref="Arrow"/> type that will be used in the game.
        /// </summary>
        public static ArrowType SelectedArrowType { get; set; }

        /// <summary>
        /// Gets or sets the music that is playing in the background.
        /// </summary>
        public static SoundPlayer Music { get; set; }

        /// <summary>
        /// Gets or sets the position of the mouse pointer.
        /// </summary>
        public Point MousePosition { get; set; }

        private Size Bounds
        {
            get { return new Size(this.Game.ActualWidth, this.Game.ActualHeight); }
        }

        private ArcherGameVisual Game { get; set; }

        /// <summary>
        /// Sets the mouse's position when called.
        /// </summary>
        /// <param name="e"><see cref="MouseEventArgs"/></param>
        public void OnMouseMove(MouseEventArgs e)
        {
            this.MousePosition = e.GetPosition(this.Game);
        }

        /// <summary>
        /// Sets the suitable button property when being presed.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e"><see cref="KeyEventArgs"/></param>
        public void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Left || e.Key == Key.A)
            {
                Left = true;
            }
            else if (e.Key == Key.Right || e.Key == Key.D)
            {
                Right = true;
            }
            else if (e.Key == Key.Up || e.Key == Key.W)
            {
                if (Up == 0)
                {
                    Up = 1;
                }
            }
            else if (e.Key == Key.LeftShift || e.Key == Key.RightShift)
            {
                Dodge = true;
            }
        }

        /// <summary>
        /// Sets the suitable button property when being released.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e"><see cref="KeyEventArgs"/></param>
        public void OnKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Left || e.Key == Key.A)
            {
                Left = false;
            }
            else if (e.Key == Key.Right || e.Key == Key.D)
            {
                Right = false;
            }
            else if (e.Key == Key.LeftShift || e.Key == Key.RightShift)
            {
                Dodge = false;
            }
        }

        /// <summary>
        /// Sets the <see cref="ShootArrow"/> property to true.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e"><see cref="KeyEventArgs"/></param>
        public void OnLeftMouseButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (this.Game.Sw.ElapsedMilliseconds > 1000)
            {
                ShootArrow = true;
            }
        }

        /// <summary>
        /// Sets the <see cref="ArrowBack"/> property to true.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e"><see cref="KeyEventArgs"/></param>
        public void OnMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            ArrowBack = true;
        }
    }
}
