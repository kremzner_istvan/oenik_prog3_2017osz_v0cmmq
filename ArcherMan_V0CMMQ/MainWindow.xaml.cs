﻿// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ArcherMan_V0CMMQ
{
    using System.Media;
    using System.Windows;
    using System.Windows.Controls;
    using ArcherMan_V0CMMQ.Elements;
    using ArcherMan_V0CMMQ.Pages;
    using ArcherMan_V0CMMQ.Pages.Levels;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
            this.SetPage(new MainMenu(this));
        }

        /// <summary>
        /// Sets the content of this <see cref="MainWindow"/> obejct to the given <see cref="Page"/>.
        /// </summary>
        /// <param name="page">The new content of this <see cref="MainWindow"/> object.</param>
        public void SetPage(Page page)
        {
            if (this.Content != null)
            {
                this.KeyDown -= (this.Content as IScreen).OnKeyDown;
                this.KeyUp -= (this.Content as IScreen).OnKeyUp;
                this.MouseLeftButtonUp -= (this.Content as IScreen).OnMouseLeftButtonUp;
                this.MouseRightButtonDown -= (this.Content as IScreen).OnMouseRightButtonDown;
            }

            if ((page is MainMenu || page is OptionsMenu)
                && !(this.Content is MainMenu || this.Content is OptionsMenu))
            {
                ViewModel.Music = new SoundPlayer(Properties.Resources.OriMainTheme);
                ViewModel.Music.Play();
            }

            if (page is Level)
            {
                ViewModel.Music = new SoundPlayer(Properties.Resources.TowerFallMusic);
                ViewModel.Music.Play();
            }

            this.Content = page;
        }
    }
}
