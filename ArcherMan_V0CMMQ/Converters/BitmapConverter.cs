﻿// <copyright file="BitmapConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ArcherMan_V0CMMQ.Converters
{
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Converts to <see cref="Bitmap"/> or to <see cref="TransformedBitmap"/>.
    /// </summary>
    public static class BitmapConverter
    {
        /// <summary>
        /// Coverts from bmp to <see cref="Bitmap"/>
        /// </summary>
        /// <param name="bmp">Bitmap file</param>
        /// <param name="format">The original format of the bitmap file</param>
        /// <returns>The Bitmap file</returns>
        public static BitmapImage ConvertToBitmapImage(Bitmap bmp, ImageFormat format)
        {
            BitmapImage image = new BitmapImage();
            MemoryStream stream = new MemoryStream();

            bmp.Save(stream, format);
            image.BeginInit();
            image.StreamSource = stream;
            image.EndInit();

            return image;
        }

        /// <summary>
        /// Makes a <see cref="TransformedBitmap"/> out of a <see cref="BitmapImage"/> and resizes it
        /// </summary>
        /// <param name="bm">The BitmapImage to rescale</param>
        /// <param name="scale">The new scale</param>
        /// <returns>A new rescaled TransformedBitmap</returns>
        public static TransformedBitmap Resize(BitmapImage bm, double scale)
        {
            return new TransformedBitmap(bm, new ScaleTransform(scale, scale));
        }

        /// <summary>
        /// Makes a <see cref="TransformedBitmap"/> out of a BitmapImage and resizes it
        /// </summary>
        /// <param name="bm">The BitmapImage to rescale</param>
        /// <param name="scaleWidth">The new scalewidth</param>
        /// <param name="scaleHeight">The new scaleheight</param>
        /// <returns>A new rescaled TransformedBitmap</returns>
        public static TransformedBitmap Resize(BitmapImage bm, double scaleWidth, double scaleHeight)
        {
            return new TransformedBitmap(bm, new ScaleTransform(scaleWidth, scaleHeight));
        }
    }
}
