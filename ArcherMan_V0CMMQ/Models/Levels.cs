﻿namespace ArcherMan_V0CMMQ.Elements
{
    using System.IO;
    using System.Reflection;

    public static class Levels
    {
        private static int index = 0;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string[,] NextLevel()
        {
            index++;
            try
            {
                Assembly assembly = Assembly.GetExecutingAssembly();
                Stream stream = assembly.GetManifestResourceStream("Level" + index + ".txt");
                string sor;
                StreamReader sr = new StreamReader(stream);
                string[,] map;
                int[] darab = new int[2];
                darab[1] = 0;
                while ((sor = sr.ReadLine()) != null)
                {
                    darab[0] = sor.Length;
                    darab[1]++;
                }

                map = new string[darab[0], darab[1]];
                sr = new StreamReader(stream);
                darab[0] = 0;
                while ((sor = sr.ReadLine()) != null)
                {
                    darab[0]++;
                    for (int i = 0; i < sor.Length; i++)
                    {
                        map[darab[0], i] = sor[i] + string.Empty;
                    }
                }

                return map;
            }
            catch (FileNotFoundException)
            {
                string[,] win = new string[1, 1];
                win[0, 0] = "You won!";
                return win;
            }
        }
    }
}
