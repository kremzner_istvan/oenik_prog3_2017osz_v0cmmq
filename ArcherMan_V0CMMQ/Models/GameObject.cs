﻿// <copyright file="GameObject.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ArcherMan_V0CMMQ.Elements
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using ArcherMan_V0CMMQ.View;

    /// <summary>
    /// The abstract game object that are placed in the level.
    /// </summary>
    public abstract class GameObject : Bindable
    {
        private Point position;
        private Geometry geometry;
        private double angle;
        private BitmapImage originalBody;
        private TransformedBitmap body;
        private Rect elementRect;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameObject"/> class.
        /// Sets the game to the current and the angle to 0.
        /// </summary>
        /// <param name="game">the current <see cref="ArcherGameVisual"/> that is being used.</param>
        protected GameObject(ArcherGameVisual game)
        {
            this.Game = game;

            this.Angle = 0;
        }

        /// <summary>
        /// Gets the  BitmapImage of this <see cref="GameObject"/> that is stored in <see cref="body"/>.
        /// </summary>
        public virtual ImageSource Sprite
        {
            get { return this.Body; }
        }

        /// <summary>
        /// Gets the width and height of the current <see cref="ArcherGameVisual"/> that represents the current level.
        /// </summary>
        public Size MapBounds
        {
            get { return new Size(this.Game.ActualWidth, this.Game.ActualHeight); }
        }

        /// <summary>
        /// Gets the geometry of this <see cref="GameObject"/> with the <see cref="TranslateTransform"/> applied with the <see cref="Position"/>.
        /// </summary>
        public virtual Geometry GetGeometry
        {
            get
            {
                this.Geometry = new RectangleGeometry(this.ElementRect);
                this.Geometry.Transform = new TranslateTransform(this.Position.X, this.Position.Y);
                return this.Geometry;
            }
        }

        /// <summary>
        /// Gets the <see cref="RotateTransform"/> of this <see cref="GameObject"/> where the angle is from <see cref="angle"/>.
        /// </summary>
        public RotateTransform GetRotation
        {
            get
            {
                return new RotateTransform(this.Angle, this.CenterOfRotation.X, this.CenterOfRotation.Y);
            }
        }

        /// <summary>
        /// Gets or sets the center of the rotation of this <see cref="GameObject"/>.
        /// </summary>
        protected virtual Point CenterOfRotation { get; set; }

        /// <summary>
        /// Gets the object's <see cref="ArcherGameVisual"/> reference.
        /// </summary>
        protected ArcherGameVisual Game { get; private set; }

        /// <summary>
        /// Gets or sets the position of this <see cref="GameObject"/>.
        /// </summary>
        protected Point Position
        {
            get
            {
                return this.position;
            }

            set
            {
                this.position = value;
            }
        }

        /// <summary>
        /// Gets or sets the geometry of this <see cref="GameObject"/>.
        /// </summary>
        protected Geometry Geometry
        {
            get
            {
                return this.geometry;
            }

            set
            {
                this.geometry = value;
            }
        }

        /// <summary>
        /// Gets or sets the angle of this <see cref="GameObject"/>.
        /// </summary>
        protected double Angle
        {
            get
            {
                return this.angle;
            }

            set
            {
                this.angle = value;
            }
        }

        /// <summary>
        /// Gets or sets the untransformed BitmapImage of this <see cref="GameObject"/>.
        /// </summary>
        protected BitmapImage OriginalBody
        {
            get
            {
                return this.originalBody;
            }

            set
            {
                this.originalBody = value;
            }
        }

        /// <summary>
        /// Gets or sets the transformed <see cref="BitmapImage"/> of this <see cref="GameObject"/> as a <see cref="TransformedBitmap"/>.
        /// </summary>
        protected TransformedBitmap Body
        {
            get
            {
                return this.body;
            }

            set
            {
                this.body = value;
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="Rect"/> that represents this <see cref="GameObject"/>.
        /// </summary>
        protected Rect ElementRect
        {
            get
            {
                return this.elementRect;
            }

            set
            {
                this.elementRect = value;
            }
        }

        /// <summary>
        /// Does the frist <see cref="GameObject"/> collide with the second <see cref="GameObject"/>?
        /// </summary>
        /// <param name="first">The first <see cref="Game"/></param>
        /// <param name="second">The second <see cref="GameObject"/></param>
        /// <returns>Whether the two <see cref="GameObject"/>s collide.</returns>
        public static bool Collision(GameObject first, GameObject second) // két objektum ütközik-e
        {
            PathGeometry intersection = Geometry.Combine(
                first.GetGeometry.GetFlattenedPathGeometry(),
                second.GetGeometry.GetFlattenedPathGeometry(),
                GeometryCombineMode.Intersect,
                null);

            return intersection.GetArea() > 0;
        }

        /// <summary>
        /// Does this <see cref="GameObject"/> collide with the given <see cref="Wall"/>s?
        /// </summary>
        /// <param name="walls">The <see cref="Wall"/>s that the <see cref="GameObject"/> may collide with.</param>
        /// <param name="other">The <see cref="GameObject"/>.</param>
        /// <returns>Whether the <see cref="GameObject"/> collides with any of the given <see cref="Wall"/>s.</returns>
        public static bool CollideWithWall(LinkedList<Wall> walls, GameObject other) // ütközik-e egy objektum a falakkal
        {
            foreach (Wall actWall in walls)
            {
                if (GameObject.Collision(actWall, other))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Does this <see cref="GameObject"/> collide with an other?
        /// </summary>
        /// <param name="other">The other <see cref="GameObject"/></param>
        /// <returns>Wheter the two <see cref="GameObject"/> collide.</returns>
        public virtual bool Collision(GameObject other) // ütközés egy másik objektummal
        {
            PathGeometry intersection = Geometry.Combine(
                this.GetGeometry.GetFlattenedPathGeometry(),
                other.GetGeometry.GetFlattenedPathGeometry(),
                GeometryCombineMode.Intersect,
                null);

            return intersection.GetArea() > 0;
        }

        /// <summary>
        /// Does this <see cref="GameObject"/> collide with an other? And if yes how big is the joint wall piece?
        /// </summary>
        /// <param name="other">The other <see cref="GameObject"/></param>
        /// <param name="hit">The size of the collided part.</param>
        /// <returns>Wheter the two <see cref="GameObject"/> collide.</returns>
        public virtual bool Collision(GameObject other, out Rect hit) // ütközés egy másik objektummal és az ütközött terület méretének visszaadása
        {
            PathGeometry intersection = Geometry.Combine(
                this.GetGeometry.GetFlattenedPathGeometry(),
                other.GetGeometry.GetFlattenedPathGeometry(),
                GeometryCombineMode.Intersect,
                null);

            hit = default(Rect);

            if (intersection.GetArea() > 0)
            {
                hit = intersection.Bounds;
            }

            return intersection.GetArea() > 0;
        }

        /// <summary>
        /// Does this <see cref="GameObject"/> collide with the given <see cref="Geometry"/> ?
        /// </summary>
        /// <param name="first">The other <see cref="GameObject"/></param>
        /// <param name="geo">The given <see cref="Geometry"/></param>
        /// <returns>Wheter the two <see cref="GameObject"/> collide.</returns>
        public virtual bool Collision(GameObject first, Geometry geo) // egy objektum és egy geometria ütközik-e
        {
            PathGeometry intersection = Geometry.Combine(
                first.GetGeometry.GetFlattenedPathGeometry(),
                geo.GetFlattenedPathGeometry(),
                GeometryCombineMode.Intersect,
                null);

            return intersection.GetArea() > 0;
        }

        /// <summary>
        /// Gets the rotation <see cref="Vector"/> of this <see cref="GameObject"/>.
        /// </summary>
        /// <param name="angle">The angle of rotation.</param>
        /// <returns>The roation <see cref="Vector"/>.</returns>
        protected static Vector GetRotationVector(double angle)
        {
            Point p = new Point(1, 0);
            return new Vector()
            {
                X = (p.X * Math.Cos(angle)) - (p.Y * Math.Sign(angle)),
                Y = (p.Y * Math.Cos(angle)) + (p.X * Math.Sin(angle))
            };
        }

        /// <summary>
        /// Linear interpolation
        /// </summary>
        /// <param name="a">Starting value</param>
        /// <param name="b">Maximal value</param>
        /// <param name="t">The percentage of change</param>
        /// <returns>The new calculated value.</returns>
        protected static double Lerp(double a, double b, double t) /*Linear interpolation*/
        {
            return a + ((b - a) * t);
        }

        /// <summary>
        /// Linear interpolation with <see cref="Point"/>
        /// </summary>
        /// <param name="a">Starting Point value</param>
        /// <param name="b">Maximal Point value</param>
        /// <param name="t">The percentage of change</param>
        /// <returns>The new calculated Point value.</returns>
        private static Point Lerp(Point a, Point b, double t) /*Lerp egy pontra*/
        {
            return new Point()
            {
                X = Lerp(a.X, b.X, t),
                Y = Lerp(a.Y, b.Y, t)
            };
        }
    }
}
