﻿// <copyright file="Wall.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ArcherMan_V0CMMQ
{
    using System.Drawing.Imaging;
    using System.Windows;
    using ArcherMan_V0CMMQ.Converters;
    using ArcherMan_V0CMMQ.Elements;
    using ArcherMan_V0CMMQ.View;

    /// <summary>
    /// The representation of a wall.
    /// </summary>
    public class Wall : GameObject
    {
        /// <summary>
        /// The number of walls vertically.
        /// </summary>
        public const double VERTICALLY = 16 * 3;

        /// <summary>
        /// The number of walls horizontally
        /// </summary>
        public const double HORIZONTALLY = 9 * 3;

        /// <summary>
        /// Initializes a new instance of the <see cref="Wall"/> class.
        /// </summary>
        /// <param name="width">The width of this wall and the width scale of the its image.</param>
        /// <param name="height">The widht of this wall anf the height scale of its image.</param>
        /// <param name="x">The x parameter of its position.</param>
        /// <param name="y">The y parameter of its poition.</param>
        /// <param name="game">The current <see cref="ArcherGameVisual"/> that represents the current level.</param>
        public Wall(double width, double height, double x, double y, ArcherGameVisual game)
            : base(game)
        {
            this.OriginalBody = BitmapConverter.ConvertToBitmapImage(ArcherMan_V0CMMQ.Properties.Resources.WoodDarkBrown, ImageFormat.Png);
            this.Body = BitmapConverter.Resize(this.OriginalBody, width / this.OriginalBody.Width, height / this.OriginalBody.Height);
            this.ElementRect = new Rect(0, 0, this.Body.Width, this.Body.Height);
            this.Position = new Point(x, y);
        }
    }
}
