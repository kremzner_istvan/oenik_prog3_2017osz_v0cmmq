﻿// <copyright file="Quicker.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ArcherMan_V0CMMQ.Models.MovingElements.Enemy
{
    using System.Drawing.Imaging;
    using ArcherMan_V0CMMQ.Converters;
    using ArcherMan_V0CMMQ.Elements;
    using ArcherMan_V0CMMQ.View;

    /// <summary>
    /// Basically a ghost just quicker <see cref="Ghost"/>.
    /// </summary>
    public sealed class Quicker : Ghost
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Quicker"/> class.
        /// </summary>
        /// <param name="game">The current <see cref="ArcherGameVisual"/> that reresent the Level.</param>
        /// <param name="portal">The sapwning <see cref="Portal"/></param>
        /// <param name="player">The current <see cref="Player"/></param>
        public Quicker(ArcherGameVisual game, Portal portal, Player player)
            : base(game, portal, player)
        {
            this.OriginalBody = BitmapConverter.ConvertToBitmapImage(ArcherMan_V0CMMQ.Properties.Resources.Quicker, ImageFormat.Png);
            this.Body = BitmapConverter.Resize(this.OriginalBody, 0.1 * this.MapBounds.Width / 1600);

            this.DynPosition = portal.GetPosition;
            this.Speed = 26;
        }
    }
}
