﻿// <copyright file="Portal.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ArcherMan_V0CMMQ.Models.MovingElements.Enemy
{
    using System.Drawing.Imaging;
    using System.Windows;
    using ArcherMan_V0CMMQ.Converters;
    using ArcherMan_V0CMMQ.Elements;
    using ArcherMan_V0CMMQ.View;

    /// <summary>
    /// These are the spawning points of the <see cref="EnemyObject"/>s.
    /// </summary>
    public sealed class Portal : GameObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Portal"/> class.
        /// </summary>
        /// <param name="game">The current <see cref="ArcherGameVisual"/> that represents the current level.</param>
        /// <param name="start">The point where this <see cref="Portal"/> will be.</param>
        public Portal(ArcherGameVisual game, Point start)
            : base(game)
        {
            this.Position = start;
            this.OriginalBody = BitmapConverter.ConvertToBitmapImage(Properties.Resources.PortalPurple, ImageFormat.Png);
            this.Body = BitmapConverter.Resize(this.OriginalBody, 1);
            this.ElementRect = new Rect(0, 0, this.Body.Width, this.Body.Height);
        }

        /// <summary>
        /// Gets the position of the <see cref="Player"/>.
        /// </summary>
        public Point GetPosition
        {
            get { return this.Position; }
        }
    }
}
