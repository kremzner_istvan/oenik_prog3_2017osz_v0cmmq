﻿// <copyright file="EvilCoin.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ArcherMan_V0CMMQ.Models.MovingElements.Enemy
{
    using System.Collections.Generic;
    using System.Drawing.Imaging;
    using ArcherMan_V0CMMQ.Converters;
    using ArcherMan_V0CMMQ.Elements;
    using ArcherMan_V0CMMQ.View;

    /// <summary>
    /// This type of <see cref="EnemyObject"/> follows the movement of the <see cref="Player"/> but just vertically.>
    /// </summary>
    public sealed class EvilCoin : EnemyObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EvilCoin"/> class.
        /// </summary>
        /// <param name="game">The current viusual of the game.</param>
        /// <param name="portal">The spawning portal of this <see cref="EvilCoin"/></param>
        /// <param name="player">The current <see cref="Player"/>.</param>
        public EvilCoin(ArcherGameVisual game, Portal portal, Player player)
            : base(game, portal, player)
        {
            this.OriginalBody = BitmapConverter.ConvertToBitmapImage(ArcherMan_V0CMMQ.Properties.Resources.EnemyCoin, ImageFormat.Png);
            this.Body = BitmapConverter.Resize(this.OriginalBody, 0.2 * this.MapBounds.Width / 1600);

            this.DynPosition = portal.GetPosition;
            this.Speed = 10;
        }

        /// <summary>
        /// Moves the <see cref="EvilCoin"/>.
        /// </summary>
        /// <param name="walls">The walls that this <see cref="EvilCoin"/> may collide with.</param>
        public override void Move(LinkedList<Wall> walls)
        {
            this.SetDirectionVector();
            this.MoveVertically(walls);
            if (this.IsGravityON)
            {
                this.Gravity(walls, (int)(this.Speed * 1.5), this.Acceleration);
            }
        }

        /// <summary>
        /// The rotation of this <see cref="EvilCoin"/> object, but it is never rotated.
        /// </summary>
        public override void Rotate()
        {
        }
    }
}
