﻿// <copyright file="Ghost.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ArcherMan_V0CMMQ.Models.MovingElements.Enemy
{
    using System.Collections.Generic;
    using System.Drawing.Imaging;
    using ArcherMan_V0CMMQ.Converters;
    using ArcherMan_V0CMMQ.Elements;
    using ArcherMan_V0CMMQ.View;

    /// <summary>
    /// This type of <see cref="EnemyObject"/> goes through the <see cref="Wall"/>s and follows the <see cref="Player"/>.
    /// </summary>
    public class Ghost : EnemyObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Ghost"/> class.
        /// </summary>
        /// <param name="game">The current <see cref="ArcherGameVisual"/> that reresent the Level.</param>
        /// <param name="portal">The sapwning <see cref="Portal"/></param>
        /// <param name="player">The current <see cref="Player"/></param>
        public Ghost(ArcherGameVisual game, Portal portal, Player player)
            : base(game, portal, player)
        {
            this.OriginalBody = BitmapConverter.ConvertToBitmapImage(ArcherMan_V0CMMQ.Properties.Resources.Ghost, ImageFormat.Png);
            this.Body = BitmapConverter.Resize(this.OriginalBody, 0.1 * this.MapBounds.Width / 1600);

            this.DynPosition = portal.GetPosition;
            this.Speed = 10;
        }

        /// <summary>
        /// The method that use to move this <see cref="Ghost"/>.
        /// </summary>
        /// <param name="walls">The <see cref="Wall"/>s that this <see cref="Ghost"/> may collide with.</param>
        public override void Move(LinkedList<Wall> walls)
        {
            this.SetDirectionVector();
            this.MoveVertically(new LinkedList<Wall>());
            this.MoveHorizontally(new LinkedList<Wall>());
        }

        /// <summary>
        /// The roation of this <see cref="Ghost"/> object, but is never used.
        /// </summary>
        public override void Rotate()
        {
        }
    }
}
