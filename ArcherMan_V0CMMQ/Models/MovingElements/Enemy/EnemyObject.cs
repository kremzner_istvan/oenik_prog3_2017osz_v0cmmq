﻿// <copyright file="EnemyObject.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ArcherMan_V0CMMQ.Models.MovingElements.Enemy
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using ArcherMan_V0CMMQ.Elements;
    using ArcherMan_V0CMMQ.Elements.Arrows;
    using ArcherMan_V0CMMQ.View;

    /// <summary>
    /// The abstratc class of the Enemies
    /// </summary>
    public abstract class EnemyObject : MovingObject
    {
        private Player player; // Aktuális játékos
        private Vector dirVector; // A játékos irányába mutató irányvektor

        /// <summary>
        /// Initializes a new instance of the <see cref="EnemyObject"/> class.
        /// </summary>
        /// <param name="game">The current <see cref="ArcherGameVisual"/> of the level.</param>
        /// <param name="portal">The spawning location of this <see cref="EnemyObject"/>.</param>
        /// <param name="player">The current <see cref="Player"/>.</param>
        public EnemyObject(ArcherGameVisual game, Portal portal, Player player)
            : base(game)
        {
            this.Player = player;
        }

        /// <summary>
        /// Gets or sets the current <see cref="Player"/> of this game.
        /// </summary>
        protected Player Player
        {
            get
            {
                return this.player;
            }

            set
            {
                this.player = value;
            }
        }

        /// <summary>
        /// Gets or sets the direction <see cref="Vector"/> of this <see cref="EnemyObject"/>.
        /// </summary>
        protected Vector DirVector
        {
            get
            {
                return this.dirVector;
            }

            set
            {
                this.dirVector = value;
            }
        }

        /// <summary>
        /// Sets the direction <see cref="Vector"/> of this <see cref="EnemyObject"/> to the normalized <see cref="Vector"/> that is made from
        /// the difference of this object's position the current <see cref="Player"/>'s position.
        /// </summary>
        protected void SetDirectionVector() /*Irányvektor beállítása*/
        {
            this.DirVector = Point.Subtract(this.Player.DynPosition, this.DynPosition);
            this.DirVector.Normalize();
        }

        /// <summary>
        /// Moves the <see cref="EnemyObject"/> vertically if it is possible. It does't let the object collide with the given <see cref="Wall"/>s.
        /// </summary>
        /// <param name="walls">The <see cref="Wall"/>s that the object may collide with.</param>
        protected virtual void MoveVertically(LinkedList<Wall> walls) // Vízszintes mozgás a játékos irányába
        {
            this.DesX = 0;

            // balra mozgás
            if (this.Player.DynPosition.X < this.DynPosition.X)
            {
                if (this.ActualOrientation == Direction.Right)
                {
                    this.Body = new TransformedBitmap(this.Body, new ScaleTransform(-1, 1));
                    this.ActualOrientation = Direction.Left;
                }

                this.DesX = -this.Speed;
            }

            // jobbra mozgás
            else if (this.Player.DynPosition.X > this.DynPosition.X)
            {
                if (this.ActualOrientation == Direction.Left)
                {
                    this.Body = new TransformedBitmap(this.Body, new ScaleTransform(-1, 1));
                    this.ActualOrientation = Direction.Right;
                }

                this.DesX = +this.Speed;
            }

            this.ElementRect = new Rect(this.DesX, 0, this.Body.Width + Math.Abs(this.DesX), this.Body.Height);
            if (!this.CollideWithWall(walls))
            {
                this.DynPosition = new Point(Lerp(this.DynPosition.X, this.DynPosition.X + this.DesX, this.Acceleration), this.DynPosition.Y);
            }

            this.DynPosition = this.DynPosition; /*ElementRect helyreállításak*/
        }

        /// <summary>
        /// Moves the <see cref="EnemyObject"/> horizontally if it is possible. It does't let the object collide with the given <see cref="Wall"/>s.
        /// </summary>
        /// <param name="walls">The <see cref="Wall"/>s that the object may collide with.</param>
        protected virtual void MoveHorizontally(LinkedList<Wall> walls) // Függőleges mozgás a játékos irányába
        {
            this.DesY = 0;
            if (this.Player.DynPosition.Y < this.DynPosition.Y)
            {
                this.DesY = -this.Speed;
            }
            else if (this.Player.DynPosition.Y > this.DynPosition.Y)
            {
                this.DesY = +this.Speed;
            }

            this.ElementRect = new Rect(0, this.DesY, this.Body.Width, this.Body.Height + Math.Abs(this.DesY));
            if (!this.CollideWithWall(walls))
            {
                this.DynPosition = new Point(this.DynPosition.X, Lerp(this.DynPosition.Y, this.DynPosition.Y + this.DesY, this.Acceleration));
            }

            this.DynPosition = this.DynPosition; /*ElementRect helyreállításak*/
        }
    }
}
