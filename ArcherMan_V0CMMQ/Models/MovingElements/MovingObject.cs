﻿// <copyright file="MovingObject.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ArcherMan_V0CMMQ.Models.MovingElements
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Media;
    using ArcherMan_V0CMMQ.Elements.Arrows;
    using ArcherMan_V0CMMQ.View;

    /// <summary>
    /// The abstarct class of the moving elements
    /// </summary>
    public abstract class MovingObject : DynamicObject
    {
        private int jumpHeight;

        /// <summary>
        /// Initializes a new instance of the <see cref="MovingObject"/> class.
        /// </summary>
        /// <param name="game">The current <see cref="ArcherGameVisual"/> that represent a level.</param>
        public MovingObject(ArcherGameVisual game)
            : base(game)
        {
            this.ActualOrientation = Direction.Left;
            this.JumpHeight = this.Speed * 5;
        }

        /// <summary>
        /// Gets or sets the oactual orientatinwhich direction this <see cref="MovingObject"/> is facing.
        /// </summary>
        protected Direction ActualOrientation { get; set; } /*Milyen irányba néz éppen*/

        /// <summary>
        /// Gets or sets the height of this <see cref="MovingObject"/>'s jump.
        /// </summary>
        protected int JumpHeight
        {
            get
            {
                return this.jumpHeight;
            }

            set
            {
                this.jumpHeight = value;
            }
        }

        /// <summary>
        /// Pulls down this <see cref="MovingObject"/> object with the gravity force.
        /// </summary>
        /// <param name="walls">The list of <see cref="Wall"/>s that this object may collide with.</param>
        /// <param name="speed">The force of the gravity.</param>
        /// <param name="acceleration">The acceleration used by Lerp.</param>
        protected virtual void Gravity(LinkedList<Wall> walls, int speed, double acceleration)
        {
            for (int i = speed; i > 0; i--)
            {
                this.DesY = +i;
                this.ElementRect = new Rect(0, this.DesY, this.Body.Width, this.Body.Height + this.DesY);
                if (!this.CollideWithWall(walls))
                {
                    this.DynPosition = new Point(this.DynPosition.X, Lerp(this.DynPosition.Y, this.DynPosition.Y + this.DesY, acceleration));
                    break;
                }

                this.DynPosition = this.DynPosition;
            }
        }

        /// <summary>
        /// Makes this <see cref="MovingObject"/> object jump.
        /// </summary>
        /// <param name="walls">The list of <see cref="Wall"/>s that this object may collide with.</param>
        /// <param name="speedX">The distance of the jump vertically.</param>
        /// <param name="speedY">The distance of the jump horizontally.</param>
        /// <param name="acceleration">The acceleration used by Lerp.</param>
        protected void Jump(LinkedList<Wall> walls, int speedX, int speedY, double acceleration)
        {
            double actX = this.DynPosition.X;
            double actY = this.DynPosition.Y;
            for (int i = speedY; i > 0; i--)
            {
                this.DesY = -i;
                this.ElementRect = new Rect(0, this.DesY, this.Body.Width, this.Body.Height - this.DesY);
                if (!this.CollideWithWall(walls))
                {
                    actY = Lerp(this.DynPosition.Y, this.DynPosition.Y + this.DesY, acceleration);
                    break;
                }

                this.DynPosition = this.DynPosition;
            }

            for (int i = Math.Abs(speedX); i > 0; i--)
            {
                this.DesX = i * (speedX / Math.Abs(speedX));
                this.ElementRect = new Rect(this.DesX, 0, this.Body.Width + Math.Abs(this.DesX), this.Body.Height);
                if (!this.CollideWithWall(walls))
                {
                    actX = Lerp(this.DynPosition.X, this.DynPosition.X + this.DesX, acceleration);
                    break;
                }

                this.DynPosition = this.DynPosition;
            }

            this.DynPosition = new Point(actX, actY);
        }

        /// <summary>
        /// Gets whether this <see cref="MovingObject"/> is next to walls in the given directions.
        /// </summary>
        /// <param name="walls">The list of <see cref="Wall"/>s.</param>
        /// <param name="left">Is it needed to chech left walls.</param>
        /// <param name="right">Is it needed to chech right walls.</param>
        /// <param name="under">Is it needed to chech walls under.</param>
        /// <returns>Whether there are walls in any of the given diection.</returns>
        protected bool NextToWall(LinkedList<Wall> walls, bool left, bool right, bool under)
        {
            int e = 2;
            int l = 0;
            int r = 0;
            int u = 0;

            if (left)
            {
                l = e;
            }

            if (right)
            {
                r = e;
            }

            if (left && right)
            {
                l = e;
                r = e * 2;
            }

            if (under)
            {
                u = e;
            }

            RectangleGeometry rgeometry = new RectangleGeometry(
                new Rect(this.DynPosition.X - l, this.DynPosition.Y, this.Body.Width + r, this.Body.Height + u));
            foreach (Wall actW in walls)
            {
                if (this.Collision(actW, rgeometry))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
