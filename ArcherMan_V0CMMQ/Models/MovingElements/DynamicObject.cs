﻿// <copyright file="DynamicObject.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ArcherMan_V0CMMQ.Models.MovingElements
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using ArcherMan_V0CMMQ.Elements;
    using ArcherMan_V0CMMQ.View;

    /// <summary>
    /// These objects can move on their own.
    /// </summary>
    public abstract class DynamicObject : GameObject
    {
        /// <summary>
        /// A universal speed for the <see cref="DynamicObject"/>s to set as default.
        /// </summary>
        protected const int GLOBALSPEED = 15;
        private static Random rnd = new Random();
        private int speed;
        private double acceleration;
        private double desX;
        private double desY;

        /// <summary>
        /// Initializes a new instance of the <see cref="DynamicObject"/> class, sets acceleration and speed.
        /// </summary>
        /// <param name="game">The current <see cref="ArcherGameVisual"/> object that is being used.</param>
        protected DynamicObject(ArcherGameVisual game)
            : base(game)
        {
            this.Speed = GLOBALSPEED + rnd.Next(0, 10);
            this.Acceleration = 0.4;
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="Player"/> is moved left.
        /// </summary>
        public bool Left
        {
            get { return ViewModel.Left; }
        } /*Balra mozgás*/

        /// <summary>
        /// Gets a value indicating whether the <see cref="Player"/> is moved right.
        /// </summary>
        public bool Right
        {
            get { return ViewModel.Right; }
        } /*Jobbra mozgás*/

        /// <summary>
        /// Gets or sets that the <see cref="Player"/> needs to jump.
        /// </summary>
        public byte Up
        {
            get { return ViewModel.Up; }
            set { ViewModel.Up = value; }
        } /*Felfele mozgás*/

        /// <summary>
        /// Gets a value indicating whether the gravity is turned on.
        /// </summary>
        public bool IsGravityON
        {
            get { return ViewModel.Gravity; }
        } /*Gravitáció*/

        /// <summary>
        /// Gets a value indicating whether the <see cref="Player"/> needs to dodge.
        /// </summary>
        public bool Dodge
        {
            get { return ViewModel.Dodge; }
        } /*Dodge SHIFT gombra*/

        /// <summary>
        /// Gets or sets the position of this <see cref="DynamicObject"/>.
        /// </summary>
        public virtual Point DynPosition
        {
            get
            {
                return this.Position;
            }

            protected set
            {
                this.Position = value;
                this.ElementRect = new Rect(new Point(0, 0), new Size(this.Body.Width, this.Body.Height));
            }
        }

        /// <summary>
        /// Gets or sets the speed of this <see cref="DynamicObject"/>.
        /// </summary>
        protected int Speed
        {
            get
            {
                return this.speed;
            }

            set
            {
                this.speed = value;
            }
        }

        /// <summary>
        /// Gets or sets the acceleration of this <see cref="DynamicObject"/>.
        /// </summary>
        protected double Acceleration
        {
            get
            {
                return this.acceleration;
            }

            set
            {
                this.acceleration = value;
            }
        }

        /// <summary>
        /// Gets or sets the destination Y parameter of this <see cref="DynamicObject"/>.
        /// </summary>
        protected double DesY
        {
            get
            {
                return this.desY;
            }

            set
            {
                this.desY = value;
            }
        }

        /// <summary>
        /// Gets or sets the destination X parameter of this <see cref="DynamicObject"/>.
        /// </summary>
        protected double DesX
        {
            get
            {
                return this.desX;
            }

            set
            {
                this.desX = value;
            }
        }

        /// <summary>
        /// The abstract class that will move this <see cref="DynamicObject"/>.
        /// </summary>
        /// <param name="walls">The <see cref="Wall"/>s that this <see cref="DynamicObject"/></param> may collide with.
        public abstract void Move(LinkedList<Wall> walls);

        /// <summary>
        /// Calculates the rotation angle of this <see cref="DynamicObject"/>.
        /// </summary>
        public abstract void Rotate();

        /// <summary>
        /// Does this <see cref="DynamicObject"/> collide with any of the given <see cref="Wall"/>s.
        /// </summary>
        /// <param name="walls">The linkedlist of <see cref="Wall"/>s.</param>
        /// <returns>Wheter any of these given walls and this object collide.</returns>
        protected bool CollideWithWall(LinkedList<Wall> walls)
        {
            foreach (Wall actWall in walls)
            {
                if (this.Collision(actWall))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Does this <see cref="DynamicObject"/> collide with any of the given <see cref="Wall"/>s.
        /// </summary>
        /// <param name="walls">The linkedlist of <see cref="Wall"/>s.</param>
        /// <param name="hit">The size of the collided parts of the two objects.</param>
        /// <returns>Wheter any of these given walls and this object collide.</returns>
        protected bool CollideWithWall(LinkedList<Wall> walls, out Rect hit)
        {
            hit = default(Rect);
            foreach (Wall actWall in walls)
            {
                if (this.Collision(actWall, out hit))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
