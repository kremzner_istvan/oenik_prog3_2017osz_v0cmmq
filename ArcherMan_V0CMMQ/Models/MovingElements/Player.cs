﻿// <copyright file="Player.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ArcherMan_V0CMMQ.Elements
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Drawing.Imaging;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using ArcherMan_V0CMMQ.Converters;
    using ArcherMan_V0CMMQ.Elements.Arrows;
    using ArcherMan_V0CMMQ.Models.MovingElements;
    using ArcherMan_V0CMMQ.View;

    /// <summary>
    /// The representation of the player
    /// </summary>
    public sealed class Player : MovingObject
    {
        private bool onLeftWall = false; /*Falon van-e balról*/
        private bool onRightWall = false; /*Falon van-e jobbról*/
        private Stopwatch sw;
        private long previousTime;

        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// </summary>
        /// <param name="game">The current <see cref="ArcherGameVisual"/>.</param>
        /// <param name="start">The starting <see cref="Point"/> of the this <see cref="Player"/>.</param>
        public Player(ArcherGameVisual game, Point start)
            : base(game)
        {
            this.OriginalBody = BitmapConverter.ConvertToBitmapImage(ArcherMan_V0CMMQ.Properties.Resources.blue2, ImageFormat.Png);
            this.Body = BitmapConverter.Resize(this.OriginalBody, 0.6 * this.MapBounds.Width / 1600);
            this.sw = new Stopwatch();
            this.sw.Start();
            this.previousTime = -1500;
            this.DynPosition = start;
            this.Speed = 22;
        }

        /// <summary>
        /// Gets or sets the current <see cref="Arrow"/> that the <see cref="Player"/> is using.
        /// </summary>
        public Arrow UsedArrow { get; set; }

        /// <summary>
        /// Gets or sets the position of this <see cref="Player"/>.
        /// </summary>
        public override Point DynPosition
        {
            get
            {
                return base.DynPosition;
            }

            protected set
            {
                if (this.UsedArrow != null)
                {
                    this.UsedArrow.SetPosition = value;
                }

                base.DynPosition = value;
                this.ElementRect = new Rect(new Point(0, 0), new Size(this.Body.Width, this.Body.Height));
            }
        }

        private bool OnWall
        {
            get
            {
                return this.onLeftWall || this.onRightWall;
            }

            set
            {
                this.onLeftWall = value;
                this.onRightWall = value;
            }
        } /*Falon van-e*/

        /// <summary>
        /// Moves this <see cref="Player"/>., may collid
        /// </summary>
        /// <param name="walls">The <see cref="Wall"/> that the player may collide with.</param>
        public override void Move(LinkedList<Wall> walls)
        {
            this.DesX = 0;
            this.DesY = 0;
            bool wallfromLeft = this.NextToWall(walls, true, false, false);
            bool wallfromRight = this.NextToWall(walls, false, true, false);
            bool wallfromUnder = this.NextToWall(walls, false, false, true);
            if (this.Left)
            {
                if (this.onRightWall)
                {
                    this.OnWall = false;
                }

                if (this.ActualOrientation == Direction.Right)
                {
                    this.Body = new TransformedBitmap(this.Body, new ScaleTransform(-1, 1));
                    this.ActualOrientation = Direction.Left;
                }

                for (int i = this.Speed; i > 0; i--)
                {
                    this.DesX = -i;
                    this.ElementRect = new Rect(this.DesX, 0, this.Body.Width - this.DesX, this.Body.Height);
                    if (!this.CollideWithWall(walls))
                    {
                        this.DynPosition = new Point(Lerp(this.DynPosition.X, this.DynPosition.X + this.DesX, this.Acceleration), this.DynPosition.Y);
                        break;
                    }

                    this.DynPosition = this.DynPosition; /*ElementRect helyreállításak*/
                }
            }

            if (this.Right)
            {
                if (this.onLeftWall)
                {
                    this.OnWall = false;
                }

                if (this.ActualOrientation == Direction.Left)
                {
                    this.Body = new TransformedBitmap(this.Body, new ScaleTransform(-1, 1));
                    this.ActualOrientation = Direction.Right;
                }

                for (int i = this.Speed; i > 0; i--)
                {
                    this.DesX = +i;
                    this.ElementRect = new Rect(this.DesX, 0, this.Body.Width + this.DesX, this.Body.Height);
                    if (!this.CollideWithWall(walls))
                    {
                        this.DynPosition = new Point(Lerp(this.DynPosition.X, this.DynPosition.X + this.DesX, this.Acceleration), this.DynPosition.Y);
                        break;
                    }

                    this.DynPosition = this.DynPosition;
                }
            }

            if (this.Up == 3 || (this.Up == 2 && wallfromUnder))
            {
                if (this.Up != 3)
                {
                    this.OnWall = false;
                }

                this.Up = 0;
            }

            if (this.Up == 1)
            {
                if (this.Up == 1)
                {
                    this.Jump(walls, 0, this.JumpHeight, this.Acceleration * 2);
                    this.Up = 2;
                }
                else if (this.Up == 2)
                {
                    if (this.OnWall)
                    {
                        if (this.onLeftWall)
                        {
                            this.Jump(walls, this.Speed, this.JumpHeight, this.Acceleration);
                        }
                        else
                        {
                            this.Jump(walls, -this.Speed, this.JumpHeight, this.Acceleration);
                        }

                        this.Up = 3;
                    }
                }

                this.OnWall = false;
            }
            else if (this.IsGravityON)
            {
                if (!this.OnWall)
                {
                    if (this.NextToWall(walls, true, false, false) && this.Left)
                    {
                        this.Gravity(walls, this.Speed, this.Acceleration);
                        this.onLeftWall = true;
                        this.Up = 3;
                    }
                    else if (this.NextToWall(walls, false, true, false) && this.Right)
                    {
                        this.Gravity(walls, this.Speed, this.Acceleration);
                        this.onRightWall = true;
                        this.Up = 3;
                    }
                    else
                    {
                        this.Gravity(walls, (int)(this.Speed * 1.5), this.Acceleration);
                    }
                }
                else
                {
                    this.Gravity(walls, (int)(this.Speed * 0.3), this.Acceleration);
                }
            }

            if (this.Dodge)
            {
                if (this.previousTime + 1500 < this.sw.ElapsedMilliseconds)
                {
                    sbyte sign;

                    if (this.ActualOrientation == Direction.Left)
                    {
                        sign = -1;
                    }
                    else
                    {
                        sign = +1;
                    }

                    for (int i = this.Speed; i > 0; i--)
                    {
                        this.DesX = +i * sign;
                        this.ElementRect = new Rect(this.DesX, 0, this.Body.Width + Math.Abs(-this.DesX), this.Body.Height);
                        if (!this.CollideWithWall(walls))
                        {
                            this.DynPosition = new Point(Lerp(this.DynPosition.X, this.DynPosition.X + this.DesX, this.Acceleration * 1.2), this.DynPosition.Y);
                            break;
                        }

                        this.DynPosition = this.DynPosition;
                        this.previousTime = this.sw.ElapsedMilliseconds;
                    }
                }
            }
        }

        /// <summary>
        /// The rotation of this <see cref="Player"/>, but it is never rotated.
        /// </summary>
        public override void Rotate()
        {
        }

        /// <summary>
        /// Pulls down this <see cref="Player"/> object with the gravity force.
        /// </summary>
        /// <param name="walls">The list of <see cref="Wall"/>s that this object may collide with.</param>
        /// <param name="speed">The force of the gravity.</param>
        /// <param name="acceleration">The acceleration used by Lerp.</param>
        protected override void Gravity(LinkedList<Wall> walls, int speed, double acceleration)
        {
            if (!this.OnWall)
            {
                for (int i = speed; i > 0; i--)
                {
                    this.DesY = +i;
                    this.ElementRect = new Rect(0, this.DesY, this.Body.Width, this.Body.Height + this.DesY);
                    if (!this.CollideWithWall(walls))
                    {
                        this.DynPosition = new Point(this.DynPosition.X, Lerp(this.DynPosition.Y, this.DynPosition.Y + this.DesY, acceleration));
                        break;
                    }

                    this.DynPosition = this.DynPosition;
                }
            }
            else
            {
                for (int i = speed; i > 0; i--)
                {
                    this.DesY = i;
                    this.ElementRect = new Rect(0, this.DesY, this.Body.Width, this.Body.Height + this.DesY);
                    if (!this.CollideWithWall(walls) && (this.OnWall = this.NextToWall(walls, true, true, false)))
                    {
                        this.DynPosition = new Point(this.DynPosition.X, Lerp(this.DynPosition.Y, this.DynPosition.Y + this.DesY, acceleration));
                        break;
                    }

                    this.DynPosition = this.DynPosition;
                }
            }
        }
    }
}
