﻿// <copyright file="Bindable.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ArcherMan_V0CMMQ.Elements
{
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Event and method for binding.
    /// </summary>
    public class Bindable : INotifyPropertyChanged
    {
        /// <summary>
        /// Property changed envent handler.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Porpertychanged's method.
        /// </summary>
        /// <param name="name">String parameter</param>
        protected void OnPropertyChanged([CallerMemberName] string name = "")
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
