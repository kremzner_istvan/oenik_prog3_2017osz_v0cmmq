﻿// <copyright file="CustomButton.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ArcherMan_V0CMMQ
{
    using System;
    using System.Drawing.Imaging;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Threading;
    using ArcherMan_V0CMMQ.Converters;

    /// <summary>
    /// The buttons that are used in the <see cref="MainMenu"/>.
    /// </summary>
    public sealed class CustomButton : Grid
    {
        // textures:
        private BitmapImage texNormal;
        private BitmapImage texHover;
        private BitmapImage texHoverOnly;
        private BitmapImage texOnClick;
        private Image hoverImage;
        private double hoverAlpha = 0f;
        private float pulseSpeed = 2f;
        private Label labelText;

        // timer:
        private DispatcherTimer dt;

        // flags:
        private bool isMouseHovering = false;
        private bool isMouseBeignPressed = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomButton"/> class.
        /// </summary>
        public CustomButton()
        {
            this.dt = new DispatcherTimer();
            this.dt.Interval = new TimeSpan(0, 0, 0, 0, 30);
            this.dt.Tick += this.Update;
            this.dt.Start();
            this.texNormal = BitmapConverter.ConvertToBitmapImage(Properties.Resources.scifiButton_normal_new, ImageFormat.Png);

            // tex_normal = new BitmapImage(new Uri(@"pack://application:,,,/Resources/Buttons/scifiButton_normal.png"));
            this.texHover = BitmapConverter.ConvertToBitmapImage(Properties.Resources.scifiButton_hover_neg, ImageFormat.Png);

            // tex_hover = new BitmapImage(new Uri(@"pack://application:,,,/Resources/Buttons/scifiButton_hover.png"));
            this.texHoverOnly = BitmapConverter.ConvertToBitmapImage(Properties.Resources.scifiButton_hover_only_neg, ImageFormat.Png);

            // tex_hoverOnly = new BitmapImage(new Uri(@"pack://application:,,,/Resources/Buttons/scifiButton_hover_only.png"));
            this.texOnClick = BitmapConverter.ConvertToBitmapImage(Properties.Resources.scifiButton_clicked_neg, ImageFormat.Png);

            // tex_onClick = new BitmapImage(new Uri(@"pack://application:,,,/Resources/Buttons/scifiButton_clicked.png"));
            this.Background = new ImageBrush(this.texNormal);
            this.Width = 260;
            this.Height = 80;
            this.hoverImage = new Image()
            {
                Source = this.texHoverOnly,
                Opacity = 0f
            };
            base.Children.Add(this.hoverImage);

            this.labelText = new Label()
            {
                FontSize = 20,
                Foreground = Brushes.DarkOrange,
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                HorizontalContentAlignment = HorizontalAlignment.Center,
                VerticalContentAlignment = VerticalAlignment.Center,
                Padding = new Thickness(10, 10, 10, 16)
            };
            base.Children.Add(this.labelText);
            this.MouseDown += this.OnMouseDown;
            this.MouseUp += this.OnMouseUp;
            this.MouseEnter += this.OnMouseEnter;
            this.MouseLeave += this.OnMouseLeave;
        }

        /// <summary>
        /// The event when clicked on the button.
        /// </summary>
        public event MouseButtonEventHandler Click;

        /// <summary>
        /// Gets the Children.
        /// </summary>
        [Obsolete("Use Text property instead.", true)]
        public new UIElementCollection Children
        {
            get; private set;
        }

        /// <summary>
        /// Gets or sets the text of the button.
        /// </summary>
        public string Text
        {
            get
            {
                return (string)this.labelText.Content;
            }

            set
            {
                this.labelText.Content = value;
            }
        }

        /// <summary>
        /// Gets or sets the pulsation speed when the mousepointer is hovering above the button.
        /// </summary>
        public float HoverPulseSpeed
        {
            get
            {
                return this.pulseSpeed;
            }

            set
            {
                this.pulseSpeed = Math.Abs(value);
            }
        }

        private double HoverAlpha
        {
            set
            {
                if (this.hoverAlpha != value)
                {
                    this.hoverAlpha = value;
                    this.hoverImage.Opacity = this.hoverAlpha;
                }
            }
        }

        private bool IsMouseBeignPressed
        {
            set
            {
                if (this.isMouseBeignPressed != value)
                {
                    this.isMouseBeignPressed = value;
                    if (value)
                    {
                        this.Background = new ImageBrush(this.texOnClick);
                    }
                    else
                    {
                        this.Background = new ImageBrush(this.texNormal);
                    }
                }
            }
        }

        private void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            this.IsMouseBeignPressed = true;

            if (this.Click != null)
            {
                this.Click(sender, e);
            }
        }

        private void OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            this.IsMouseBeignPressed = false;
        }

        private void OnMouseEnter(object sender, MouseEventArgs e)
        {
            this.isMouseHovering = true;
        }

        private void OnMouseLeave(object sender, MouseEventArgs e)
        {
            this.isMouseHovering = false;
        }

        private void Update(object sender, EventArgs e)
        {
            if (this.isMouseHovering && !this.isMouseBeignPressed)
            {
                this.HoverAlpha = (Math.Abs(Math.Sin(TimeSpan.FromTicks(DateTime.Now.Ticks).TotalSeconds * this.pulseSpeed)) / 2) + 0.5f;
            }
            else
            {
                this.HoverAlpha = 0f;
            }
        }
    }
}
