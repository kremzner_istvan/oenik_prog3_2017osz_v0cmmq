﻿// <copyright file="PointerFollowingArrow.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ArcherMan_V0CMMQ.Models.Arrows
{
    using System.Collections.Generic;
    using System.Drawing.Imaging;
    using System.Windows;
    using ArcherMan_V0CMMQ.Converters;
    using ArcherMan_V0CMMQ.Elements;
    using ArcherMan_V0CMMQ.Elements.Arrows;
    using ArcherMan_V0CMMQ.View;

    /// <summary>
    /// This type of <see cref="Arrow"/> follows the cursor even after shot and can be reshot after hits a <see cref="Wall"/>.
    /// </summary>
    public sealed class PointerFollowingArrow : MouseDirArrow
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PointerFollowingArrow"/> class.
        /// </summary>
        /// <param name="player">The currrent player of the game and owner of this arrow.</param>
        /// <param name="game">The current visual of the active level.</param>
        public PointerFollowingArrow(Player player, ArcherGameVisual game)
            : base(player, game)
        {
            this.OriginalBody = BitmapConverter.ConvertToBitmapImage(Properties.Resources.towerfallArrowRED, ImageFormat.Png);
            this.Body = BitmapConverter.Resize(this.OriginalBody, 0.18 * this.MapBounds.Width / 1600);
            this.ElementRect = new Rect(new Point(0, 0), new Size(this.Body.Width, this.Body.Height));

            this.DirVector = Point.Subtract(
                new Point(this.DynPosition.X + this.Body.Width, this.DynPosition.Y + this.HalfSize.Height),
                new Point(this.DynPosition.X, this.DynPosition.Y + this.HalfSize.Height)); /*Az irányvektornak kezdeti értékek adása, amely a kurzor irányába mutat.*/
            this.Speed = 60;
        }

        /// <summary>
        /// The method that moves the <see cref="PointerFollowingArrow"/>.
        /// </summary>
        /// <param name="walls">The walls that the arrow may collide with.</param>
        public override void Move(LinkedList<Wall> walls) /*A nyíl mozgatásáért felelős metódus*/
        {
            this.RotateCalc();
            if (this.ShootArrow)
            {
                this.Owner.UsedArrow = null;
                Point newPos = default(Point);
                if (!this.CollideWithWall(walls))
                {
                    newPos.X = Lerp(this.DynPosition.X, this.DynPosition.X + (this.Speed * this.DirVector.X / this.DirVector.Length), this.Acceleration);
                    newPos.Y = Lerp(this.DynPosition.Y, this.DynPosition.Y + (this.Speed * this.DirVector.Y / this.DirVector.Length), this.Acceleration);
                    this.DynPosition = newPos;
                }
                else
                {
                    this.ShootArrow = false;
                    this.OriginalBody = BitmapConverter.ConvertToBitmapImage(ArcherMan_V0CMMQ.Properties.Resources.towerfallArrowPURPLE, ImageFormat.Png);
                    this.Body = BitmapConverter.Resize(this.OriginalBody, 0.18 * this.MapBounds.Width / 1600);
                    this.Visibility = false;
                }
            }

            // A nyíl visszajön jobb kattintásra
            if (this.ArrowBack)
            {
                this.ArrowBack = false;
                if (this.CollideWithWall(walls))
                {
                    this.Owner.UsedArrow = this;

                    this.Visibility = true;

                    this.OriginalBody = BitmapConverter.ConvertToBitmapImage(ArcherMan_V0CMMQ.Properties.Resources.towerfallArrowRED, ImageFormat.Png);
                    this.Body = BitmapConverter.Resize(this.OriginalBody, 0.18 * this.MapBounds.Width / 1600);

                    this.DynPosition = this.Owner.DynPosition;
                }
            }
        }
    }
}
