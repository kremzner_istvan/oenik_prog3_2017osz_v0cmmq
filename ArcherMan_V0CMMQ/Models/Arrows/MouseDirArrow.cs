﻿// <copyright file="MouseDirArrow.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ArcherMan_V0CMMQ.Elements.Arrows
{
    using System;
    using System.Collections.Generic;
    using System.Drawing.Imaging;
    using System.Windows;
    using ArcherMan_V0CMMQ.Converters;
    using ArcherMan_V0CMMQ.View;

    /// <summary>
    /// This type of <see cref="Arrow"/> shoots in the direction of the cursor and can be reshot when hits a <see cref="Wall"/>.
    /// </summary>
    public class MouseDirArrow : Arrow
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MouseDirArrow"/> class.
        /// </summary>
        /// <param name="player">The Current <see cref="Player"/> who owns this arrow.</param>
        /// <param name="game">The Current <see cref="ArcherGameVisual"/> of the level.</param>
        public MouseDirArrow(Player player, ArcherGameVisual game)
            : base(player, game) /*konstruktor*/
        {
            this.OriginalBody = BitmapConverter.ConvertToBitmapImage(ArcherMan_V0CMMQ.Properties.Resources.towerfallArrowBLUE, ImageFormat.Png);
            this.Body = BitmapConverter.Resize(this.OriginalBody, 0.14 * this.MapBounds.Width / 1600);

            this.ElementRect = new Rect(new Point(0, 0), new Size(this.Body.Width, this.Body.Height));
            this.Angle = 0;

            this.DirVector = Point.Subtract(
                new Point(this.DynPosition.X + this.Body.Width, this.DynPosition.Y + (this.Body.Height / 2)),
                new Point(this.DynPosition.X, this.DynPosition.Y + (this.Body.Height / 2))); /*Az irányvektornak kezdeti értékek adása, amely a kurzor irányába mutat.*/
            this.Speed = 60;
        }

        /// <summary>
        /// Gets or sets the Shooting direction of the <see cref="MouseDirArrow"/>.
        /// </summary>
        public Direction ShootingDirection { get; set; }

        /// <summary>
        /// Gets the center of the <see cref="MouseDirArrow"/>, this will be used later to rotate it.
        /// </summary>
        protected override Point CenterOfRotation // A nyíl közepe
        {
            get
            {
                return new Point(this.DynPosition.X + (this.Body.Width / 2), this.DynPosition.Y + (this.Body.Height / 2));
            }
        }

        /// <summary>
        /// Moves the <see cref="MouseDirArrow"/> class.
        /// /// </summary>
        /// <param name="walls">The walls that the arrow may hit.</param>
        public override void Move(LinkedList<Wall> walls) /*A nyíl mozgatásáért felelős metódus*/
        {
            /*If-et kikomentelve az egeret követi a nyíl*/
            if (this.Owner.UsedArrow != null)
            {
                this.RotateCalc();
            }

            /*Ha a játékos lőtt, akkor ne legyen már a nyíl a birtokában, valamint addig haladjon az irányvektor mentén amíg falba nem ütközik.*/
            if (this.ShootArrow)
            {
                this.Owner.UsedArrow = null;
                Point newPos = default(Point);
                if (!this.CollideWithWall(walls))
                {
                    newPos.X = Lerp(this.DynPosition.X, this.DynPosition.X + (this.Speed * this.DirVector.X / this.DirVector.Length), this.Acceleration);
                    newPos.Y = Lerp(this.DynPosition.Y, this.DynPosition.Y + (this.Speed * this.DirVector.Y / this.DirVector.Length), this.Acceleration);
                    this.DynPosition = newPos;
                }
                else
                {
                    this.ShootArrow = false;
                    this.OriginalBody = BitmapConverter.ConvertToBitmapImage(Properties.Resources.towerfallArrowYELLOW, ImageFormat.Png);
                    this.Body = BitmapConverter.Resize(this.OriginalBody, 0.14 * this.MapBounds.Width / 1600);
                    this.Visibility = false;
                }
            }

            // A nyíl visszajön jobb kattintásra
            if (this.ArrowBack)
            {
                this.ArrowBack = false;
                if (this.CollideWithWall(walls))
                {
                    this.Owner.UsedArrow = this;

                    this.Visibility = true;

                    this.OriginalBody = BitmapConverter.ConvertToBitmapImage(ArcherMan_V0CMMQ.Properties.Resources.towerfallArrowBLUE, ImageFormat.Png);
                    this.Body = BitmapConverter.Resize(this.OriginalBody, 0.14 * this.MapBounds.Width / 1600);

                    this.DynPosition = this.Owner.DynPosition;
                }
            }
        }

        /// <summary>
        /// Caculates the angle that <see cref="MouseDirArrow"/> should rotate.
        /// </summary>
        public override void Rotate() /*Kiszámolja az egér pozíciójához képest az elfordulás szögét.*/
        {
            double x = this.MousePosition.X;
            double y = this.MousePosition.Y;

            double newX = Math.Abs(x - this.DynPosition.X);
            double newY = Math.Abs(y - this.DynPosition.Y);
            double powX = Math.Pow(newX, 2);
            double powY = Math.Pow(newY, 2);
            double distance = Math.Sqrt(powX + powY);
            double result = newX / distance;

            this.Quadrant(newX, newY, result);
        }

        /// <summary>
        /// Sets the rotation of this <see cref="MouseDirArrow"/> object.
        /// </summary>
        protected void RotateCalc()
        {
            if (this.ShootingDirection == Direction.None)
            {
                this.Rotate();
                this.SetDirectionVector();
            }
            else
            {
                switch (this.ShootingDirection)
                {
                    default:
                    case Direction.None:
                    case Direction.Left: this.Angle = 180; break;
                    case Direction.Right: this.Angle = 0; break;
                    case Direction.UpWard: this.Angle = 270; break;
                    case Direction.DownWard: this.Angle = 90; break;
                }
            }
        }

        /// <summary>
        /// Set the direction <see cref="Vector"/> of this <see cref="MouseDirArrow"/> class
        /// </summary>
        protected void SetDirectionVector() /*Irányvektor beállítása*/
        {
            this.DirVector = Point.Subtract(this.MousePosition, this.DynPosition);
        }

        private void Quadrant(double a, double b, double c) /*Melyik síknegyedben helyezkedik el.*/
        {
            double x = this.MousePosition.X;
            double y = this.MousePosition.Y;

            if (this.DynPosition.X < x && this.DynPosition.Y < y)
            {
                // 1. síkengyed
                this.Angle = Math.Atan(b / a) * 180 / Math.PI;
            }
            else if (this.DynPosition.X > x && this.DynPosition.Y < y)
            {
                // 2. síknegyed
                this.Angle = Math.Atan(b / a) * 180 / Math.PI;
                this.Angle = 180 - this.Angle;
            }
            else if (this.DynPosition.X > x && this.DynPosition.Y > y)
            {
                // 3. síknegyed
                this.Angle = Math.Atan(b / a) * 180 / Math.PI;
                this.Angle = 180 + this.Angle;
            }
            else if (this.DynPosition.X < x && this.DynPosition.Y > y)
            {
                // 4. síkengyed
                this.Angle = Math.Atan(b / a) * 180 / Math.PI;
                this.Angle = 360 - this.Angle;
            }
        }
    }
}
