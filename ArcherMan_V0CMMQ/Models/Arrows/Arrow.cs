﻿// <copyright file="Arrow.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ArcherMan_V0CMMQ.Elements.Arrows
{
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Media;
    using ArcherMan_V0CMMQ.Models.MovingElements;
    using ArcherMan_V0CMMQ.View;

    /// <summary>
    /// Direction of the Arrow
    /// </summary>
    public enum Direction
    {
        /// <summary>
        /// No direction given.
        /// </summary>
        None,

        /// <summary>
        /// Left direction.
        /// </summary>
        Left,

        /// <summary>
        /// Right direction.
        /// </summary>
        Right,

        /// <summary>
        /// Upward direction.
        /// </summary>
        UpWard,

        /// <summary>
        /// Downward direction.
        /// </summary>
        DownWard
    }

    /// <summary>
    /// Abstract class of the <see cref="Arrow"/>s.
    /// </summary>
    public abstract class Arrow : DynamicObject
    {
        private bool visible;

        /// <summary>
        /// Initializes a new instance of the <see cref="Arrow"/> class.
        /// </summary>
        /// <param name="player">Current player</param>
        /// <param name="game">The current visual of the level</param>
        public Arrow(Player player, ArcherGameVisual game)
            : base(game) /*konstruktor*/
        {
            this.Speed = 26;
            this.Owner = player;
            this.Visibility = false;
        }

        /// <summary>
        /// Gets or sets a value indicating whether is this <see cref="Arrow"/> shot
        /// </summary>
        public bool ShootArrow
        {
            get { return ViewModel.ShootArrow; }
            set { ViewModel.ShootArrow = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether is this <see cref="Arrow"/> called back
        /// </summary>
        public bool ArrowBack
        {
            get { return ViewModel.ArrowBack; } set { ViewModel.ArrowBack = value; }
        }

        /// <summary>
        /// Gets where the Mousepointer is currently on the screen
        /// </summary>
        public Point MousePosition
        {
            get { return this.Game.MousePosition; }
        }

        /// <summary>
        /// Sets the poosition of this <see cref="Arrow"/> if it is visible
        /// </summary>
        public Point SetPosition
        {
            set
            {
                if (this.Visibility)
                {
                    this.DynPosition = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="Arrow"/> is visible
        /// </summary>
        public bool Visibility
        {
            get
            {
                return this.visible;
            }

            set
            {
                this.visible = value;
            }
        }

        /// <summary>
        /// Gets or sets the current <see cref="Player"/> who is the owner of this <see cref="Arrow"/>.
        /// </summary>
        protected Player Owner { get; set; }

        /// <summary>
        /// Gets or sets the direction <see cref="Vector"/>.
        /// </summary>
        protected Vector DirVector { get; set; }

        /// <summary>
        /// Gets the half of the <see cref="ArcherGameVisual"/>'s dimensions.
        /// </summary>
        protected Size HalfSize
        {
            get { return new Size(this.Body.Width / 2, this.Body.Height / 2); }
        }

        /// <summary>
        /// Whether this <see cref="Arrow"/> collide with any <see cref="Wall"/> obejct-.
        /// </summary>
        /// <param name="walls">The list of <see cref="Wall"/> objects that the this <see cref="Arrow"/> may collide with.</param>
        /// <param name="hit">The size of the collided <see cref="Wall"/> obejcts' common area.</param>
        /// <returns>Whether this object collided with any of the given <see cref="Wall"/>s.</returns>
        protected bool Collision(LinkedList<Wall> walls, out Rect hit) /*Ütközik-e éppen a nyíl fallal.*/
        {
            PathGeometry intersection;
            foreach (Wall actW in walls)
            {
                intersection = Geometry.Combine(
                        this.GetGeometry.GetFlattenedPathGeometry(),
                        actW.GetGeometry.GetFlattenedPathGeometry(),
                        GeometryCombineMode.Intersect,
                        null);
                if (intersection.GetArea() > 0)
                {
                    hit = intersection.Bounds;
                    return true;
                }
            }

            hit = default(Rect);
            return false;
        }
    }
}
