﻿// <copyright file="MainMenu.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ArcherMan_V0CMMQ
{
    using System.Drawing.Imaging;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using ArcherMan_V0CMMQ.Converters;
    using ArcherMan_V0CMMQ.Pages;
    using ArcherMan_V0CMMQ.Pages.Levels;

    /// <summary>
    /// The Main Menu of the game, where the game can be started, options can be reached and could be closed.
    /// </summary>
    public partial class MainMenu : Page, IScreen
    {
        private CustomButton newGameCB;
        private CustomButton optionsCB;
        private CustomButton exitCB;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainMenu"/> class.
        /// </summary>
        /// <param name="window">Reference to the <see cref="MainWindow"/></param>
        public MainMenu(MainWindow window)
        {
            this.InitializeComponent();
            this.Main = window;
            this.Background = new ImageBrush(
                BitmapConverter.ConvertToBitmapImage(Properties.Resources.MainBackground, ImageFormat.Png));
        }

        private MainWindow Main { get; set; }

        /// <summary>
        /// Starts a new game with the given paramters, sets the main window's content to <see cref="Level"/>.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e"><see cref="KeyEventArgs"/></param>
        public void NewGameOnClick(object sender, RoutedEventArgs e)
        {
            this.Main.SetPage(new Level(this.Main));
        }

        /// <summary>
        /// Starts the options, sets the main window's content to <see cref="OptionsMenu"/>.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e"><see cref="KeyEventArgs"/></param>
        public void OptionsOnClick(object sender, RoutedEventArgs e)
        {
            this.Main.SetPage(new OptionsMenu(this.Main));
        }

        /// <summary>
        /// Shuts down the application.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e"><see cref="KeyEventArgs"/></param>
        public void ExitOnClick(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        /// <summary>
        /// When a button is being pressed. If it is escape it gets back to <see cref="MainMenu"/>.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e"><see cref="KeyEventArgs"/></param>
        public void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.N || e.Key == Key.D1)
            {
                this.NewGameOnClick(sender, new RoutedEventArgs());
            }
            else if (e.Key == Key.O || e.Key == Key.D2)
            {
                this.OptionsOnClick(sender, e);
            }
            else if (e.Key == Key.Escape || e.Key == Key.E || e.Key == Key.D3)
            {
                Application.Current.Shutdown();
            }
        }

        /// <summary>
        /// When a button is being released.
        /// </summary>
        /// <param name="sender">sedner</param>
        /// <param name="e"><see cref="KeyEventArgs"/></param>
        public void OnKeyUp(object sender, KeyEventArgs e)
        {
        }

        /// <summary>
        /// When the mouse left button is being released.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e"><see cref="KeyEventArgs"/></param>
        public void OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
        }

        /// <summary>
        /// When the mouse right button is being pressed.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e"><see cref="KeyEventArgs"/></param>
        public void OnMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
        }

        /// <summary>
        /// When the canvas is loaded loads the buttons in a stackpanel.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e"><see cref="KeyEventArgs"/></param>
        private void Canvas_Loaded(object sender, RoutedEventArgs e)
        {
            StackPanel sp = new StackPanel()
            {
                Margin = new Thickness(40, 0, 0, 0),
            };
            sp.HorizontalAlignment = HorizontalAlignment.Center;
            sp.VerticalAlignment = VerticalAlignment.Center;
            this.newGameCB = new CustomButton() { Text = "New game" };
            this.newGameCB.Click += this.NewGameOnClick;
            this.optionsCB = new CustomButton() { Text = "Options" };
            this.optionsCB.Click += this.OptionsOnClick;
            this.exitCB = new CustomButton() { Text = "Exit" };
            this.exitCB.Click += this.ExitOnClick;

            sp.Children.Add(this.newGameCB);
            sp.Children.Add(this.optionsCB);
            sp.Children.Add(this.exitCB);

            this.grid.Children.Add(sp);
            this.Main.KeyDown += this.OnKeyDown;
            this.Main.KeyUp += this.OnKeyUp;
            this.Main.MouseLeftButtonUp += this.OnMouseLeftButtonUp;
            this.Main.MouseRightButtonDown += this.OnMouseRightButtonDown;
        }
    }
}
