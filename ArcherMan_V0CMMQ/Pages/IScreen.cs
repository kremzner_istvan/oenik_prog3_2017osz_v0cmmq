﻿// <copyright file="IScreen.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ArcherMan_V0CMMQ.Pages
{
    using System.Windows.Input;

    /// <summary>
    /// This interface makes sure the Pages have the right methods.
    /// </summary>
    public interface IScreen
    {
        /// <summary>
        /// When a button is being pressed.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e"><see cref="KeyEventArgs"/></param>
        void OnKeyDown(object sender, KeyEventArgs e);

        /// <summary>
        /// When a button is being released.
        /// </summary>
        /// <param name="sender">sedner</param>
        /// <param name="e"><see cref="KeyEventArgs"/></param>
        void OnKeyUp(object sender, KeyEventArgs e);

        /// <summary>
        /// When the mouse left button is being released.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e"><see cref="KeyEventArgs"/></param>
        void OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e);

        /// <summary>
        /// When the mouse right button is being pressed.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e"><see cref="KeyEventArgs"/></param>
        void OnMouseRightButtonDown(object sender, MouseButtonEventArgs e);
    }
}
