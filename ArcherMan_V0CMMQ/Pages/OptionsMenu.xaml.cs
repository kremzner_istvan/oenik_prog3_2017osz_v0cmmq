﻿// <copyright file="OptionsMenu.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ArcherMan_V0CMMQ.Pages
{
    using System.Drawing.Imaging;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using ArcherMan_V0CMMQ.Converters;
    using ArcherMan_V0CMMQ.Elements;

    /// <summary>
    /// The Options menu where you can set the settings of the next game.
    /// </summary>
    public partial class OptionsMenu : Page, IScreen
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OptionsMenu"/> class.
        /// </summary>
        /// <param name="window">Reference to the <see cref="MainWindow"/></param>
        public OptionsMenu(MainWindow window)
        {
            this.InitializeComponent();
            this.Main = window;
            this.Background = new ImageBrush(
                BitmapConverter.ConvertToBitmapImage(Properties.Resources.OptionsBackground, ImageFormat.Png));

            this.Main.KeyDown += this.OnKeyDown;
            this.Main.KeyUp += this.OnKeyUp;
            this.Main.MouseLeftButtonUp += this.OnMouseLeftButtonUp;
            this.Main.MouseRightButtonDown += this.OnMouseRightButtonDown;
        }

        private MainWindow Main { get; set; }

        /// <summary>
        /// When a button is being pressed. If it is escape it gets back to <see cref="MainMenu"/>.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e"><see cref="KeyEventArgs"/></param>
        public void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.Main.SetPage(new MainMenu(this.Main));
            }
        }

        /// <summary>
        /// When a button is being released.
        /// </summary>
        /// <param name="sender">sedner</param>
        /// <param name="e"><see cref="KeyEventArgs"/></param>
        public void OnKeyUp(object sender, KeyEventArgs e)
        {
        }

        /// <summary>
        /// When the mouse left button is being released.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e"><see cref="KeyEventArgs"/></param>
        public void OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
        }

        /// <summary>
        /// When the mouse right button is being pressed.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e"><see cref="KeyEventArgs"/></param>
        public void OnMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
        }

        private void NormalSelected(object sender, RoutedEventArgs e)
        {
            ViewModel.SelectedArrowType = View.ArrowType.Normal;
        }

        private void FollowingSelected(object sender, RoutedEventArgs e)
        {
            ViewModel.SelectedArrowType = View.ArrowType.Following;
        }

        private void OnButtonClick(object sender, RoutedEventArgs e)
        {
            this.Main.SetPage(new MainMenu(this.Main));
        }
    }
}
