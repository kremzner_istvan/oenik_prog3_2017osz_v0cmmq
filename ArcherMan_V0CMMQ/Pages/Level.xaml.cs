﻿// <copyright file="Level.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ArcherMan_V0CMMQ.Pages.Levels
{
    using System.Drawing.Imaging;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using ArcherMan_V0CMMQ.Converters;

    /// <summary>
    /// The Level of the game with an <see cref="ArcherMan_V0CMMQ.View.ArcherGameVisual"/> set as content.
    /// </summary>
    public partial class Level : IScreen
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Level"/> class.
        /// </summary>
        /// <param name="window">Reference to the <see cref="MainWindow"/></param>
        public Level(MainWindow window)
        {
            this.InitializeComponent();
            this.Main = window;
            this.Game.Main = this.Main;
            this.Background = new ImageBrush(BitmapConverter.ConvertToBitmapImage(Properties.Resources.Mountains2, ImageFormat.Png));

            this.Main.KeyDown += this.OnKeyDown;
            this.Main.KeyUp += this.OnKeyUp;
            this.Main.MouseLeftButtonUp += this.OnMouseLeftButtonUp;
            this.Main.MouseRightButtonDown += this.OnMouseRightButtonDown;
        }

        private MainWindow Main { get; set; } // MainWindow

        /// <summary>
        /// When a button is being pressed. If it is escape it gets back to <see cref="MainMenu"/>.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e"><see cref="KeyEventArgs"/></param>
        public void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.Main.SetPage(new MainMenu(this.Main));
            }
            else
            {
                this.Game.OnKeyDown(sender, e);
            }
        }

        /// <summary>
        /// When a button is being released.
        /// </summary>
        /// <param name="sender">sedner</param>
        /// <param name="e"><see cref="KeyEventArgs"/></param>
        public void OnKeyUp(object sender, KeyEventArgs e)
        {
            this.Game.OnKeyUp(sender, e);
        }

        /// <summary>
        /// What happens when the <see cref="Level"/> is Loaded.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e"><see cref="KeyEventArgs"/></param>
        public void OnLoaded(object sender, RoutedEventArgs e)
        {
        }

        /// <summary>
        /// When the mouse left button is being released.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e"><see cref="KeyEventArgs"/></param>
        public void OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.Game.OnMouseLeftButtonUp(sender, e);
        }

        /// <summary>
        /// When the mouse right button is being pressed.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e"><see cref="KeyEventArgs"/></param>
        public void OnMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.Game.OnMouseRightButtonDown(sender, e);
        }

        /// <summary>
        /// Calls the <see cref="View.ArcherGameVisual"/>'s MouseMove method.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e"><see cref="MouseEventArgs"/></param>
        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            this.Game.OnMouseMove(sender, e);
        }
    }
}
