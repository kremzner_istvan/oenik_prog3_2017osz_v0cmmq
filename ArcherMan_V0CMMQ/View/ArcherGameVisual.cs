﻿// <copyright file="ArcherGameVisual.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ArcherMan_V0CMMQ.View
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Media;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Threading;
    using ArcherMan_V0CMMQ.Elements;
    using ArcherMan_V0CMMQ.Elements.Arrows;
    using ArcherMan_V0CMMQ.Models.Arrows;
    using ArcherMan_V0CMMQ.Models.MovingElements.Enemy;

    /// <summary>
    /// The type of <see cref="EnemyObject"/>s;
    /// </summary>
    public enum EnemyType
    {
        /// <summary>
        /// <see cref="EvilCoin"/>
        /// </summary>
        Coin,

        /// <summary>
        /// <see cref="Models.MovingElements.Enemy.Ghost"/>
        /// </summary>
        Ghost,

        /// <summary>
        /// <see cref="Models.MovingElements.Enemy.Quicker"/>
        /// </summary>
        Quicker
    }

    /// <summary>
    /// The type of <see cref="Arrow"/>s;
    /// </summary>
    public enum ArrowType
    {
        /// <summary>
        /// <see cref="MouseDirArrow"/>
        /// </summary>
        Normal,

        /// <summary>
        /// <see cref="PointerFollowingArrow"/>
        /// </summary>
        Following
    }

    /// <summary>
    /// The visual of the Level.
    /// </summary>
    public sealed class ArcherGameVisual : FrameworkElement
    {
        private const int PORTALCOUNTER = 3; // portálok száma
        private static Random rnd = new Random(); // Random számgenerátor

        private ViewModel vm; // Viewmodel aktuális példánya
        private DispatcherTimer dt; // Idözíző ami mozgatja az elemeket
        private int roundCounter = 1; // Kör száma
        private EnemyObject tempEnemy; // Enemy objektum amit ideiglenes tárolásra használok

        private LinkedList<Wall> walls;
        private LinkedList<EnemyObject> enemies;
        private LinkedList<Portal> portals;

        private Arrow actualArrow;
        private Player player;

        /// <summary>
        /// Initializes a new instance of the <see cref="ArcherGameVisual"/> class. Sets the important variabled to default.
        /// </summary>
        public ArcherGameVisual()
        {
            this.Sw = new Stopwatch();
            this.Sw.Start();
            this.Loaded += this.OnLoaded;
            this.vm = new ViewModel(this);

            this.walls = new LinkedList<Wall>();
            this.enemies = new LinkedList<EnemyObject>();
            this.portals = new LinkedList<Portal>();
        }

        /// <summary>
        /// Gets the linkedList of <see cref="Wall"/>s.
        /// </summary>
        public LinkedList<Wall> Walls
        {
            get { return this.walls; }
        }

        /// <summary>
        /// Gets the <see cref="Arrow"/> that is currently used.
        /// </summary>
        public Arrow ActualArrow
        {
            get { return this.actualArrow; }
        }

        /// <summary>
        /// Gets the current <see cref="Player"/>.
        /// </summary>
        public Player Player
        {
            get { return this.player; }
        }

        /// <summary>
        /// Gets the linkedList of <see cref="Portal"/>s.
        /// </summary>
        public LinkedList<Portal> Portals
        {
            get { return this.portals; }
        }

        /// <summary>
        /// Gets the linkedList of <see cref="EnemyObject"/>s.
        /// </summary>
        public LinkedList<EnemyObject> Enemies
        {
            get { return this.enemies; }
        }

        /// <summary>
        /// Gets a <see cref="Stopwatch"/> used to measure time.
        /// </summary>
        public Stopwatch Sw { get; private set; }

        /// <summary>
        /// Gets or sets a reference to the <see cref="MainWindow"/>.
        /// </summary>
        public MainWindow Main { get; set; }

        /// <summary>
        /// Gets the position of the mousepointer.
        /// </summary>
        public Point MousePosition
        {
            get { return this.vm.MousePosition; }
        }

        private Size Bounds
        {
            get { return new Size(this.ActualWidth, this.ActualHeight); }
        }

        /// <summary>
        /// Initializes the <see cref="Wall"/>s, the <see cref="Player"/>, the <see cref="Arrow"/> and the first <see cref="EnemyObject"/>.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e"><see cref="KeyEventArgs"/></param>
        public void OnLoaded(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this).KeyDown += this.OnKeyDown;
            Window.GetWindow(this).KeyUp += this.OnKeyUp;
            Window.GetWindow(this).MouseLeftButtonUp += this.OnMouseLeftButtonUp;
            Window.GetWindow(this).MouseRightButtonDown += this.OnMouseRightButtonDown;

            this.InitAll();
            this.DataContext = this.vm;
            this.InvalidateVisual();

            this.Sw = new Stopwatch();
            this.Sw.Start();

            this.dt = new DispatcherTimer();
            this.dt.Interval = new TimeSpan(0, 0, 0, 0, 30);
            this.dt.Tick += this.Update;
            this.dt.Start();
        }

        /// <summary>
        /// Calls <see cref="ViewModel"/>"'s method.If the button is Escape it gets back to <see cref="MainMenu"/>.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e"><see cref="KeyEventArgs"/></param>
        public void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                if (this.dt != null)
                {
                    this.dt.Tick -= this.Update;
                }

                this.Main.SetPage(new MainMenu(this.Main));
            }
            else if (e.Key == Key.Enter && this.Main.Content is MainMenu)
            {
                (this.Main.Content as MainMenu).NewGameOnClick(sender, e);
            }
            else
            {
                this.vm.OnKeyDown(sender, e);
            }
        }

        /// <summary>
        /// Calls <see cref="ViewModel"/>"'s method.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e"><see cref="KeyEventArgs"/></param>
        public void OnKeyUp(object sender, KeyEventArgs e)
        {
            this.vm.OnKeyUp(sender, e);
        }

        /// <summary>
        /// Calls <see cref="ViewModel"/>"'s method.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e"><see cref="KeyEventArgs"/></param>
        public void OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.vm.OnLeftMouseButtonUp(sender, e);
        }

        /// <summary>
        /// Calls <see cref="ViewModel"/>"'s method.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e"><see cref="KeyEventArgs"/></param>
        public void OnMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.vm.OnMouseRightButtonDown(sender, e);
        }

        /// <summary>
        /// Calls <see cref="ViewModel"/>"'s method.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e"><see cref="KeyEventArgs"/></param>
        public void OnMouseMove(object sender, MouseEventArgs e)
        {
            this.vm.OnMouseMove(e);
        }

        /// <summary>
        /// Moves the <see cref="player"/>, the <see cref="Arrow"/> and the <see cref="EnemyObject"/>. It is called every period by a <see cref="DispatcherTimer"/>.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e"><see cref="KeyEventArgs"/></param>
        public void Update(object sender, EventArgs e) /*A metódus, amelyet az időzítő mindig meghív, ez mozgatja és frissíti a játékelemeket.*/
        {
            // Játékos és nyíl mozgatása
            if (this.player != null)
            {
                this.player.Move(this.Walls);
                this.ActualArrow.Move(this.Walls);
            }

            // Ha eltalál a nyíl egy ellenséget, új ellenségek létrehozása
            if ((this.tempEnemy = this.CollideWithEnemy(this.Enemies, this.ActualArrow)) != null)
            {
                this.Enemies.Remove(this.tempEnemy);
                if (this.roundCounter <= 5)
                {
                    this.roundCounter++;

                    for (int i = 0; i < this.roundCounter; i++)
                    {
                        int chance = rnd.Next(1, 101);
                        if (chance < 47)
                        {
                            this.EnemyInit(EnemyType.Coin);
                        }
                        else if (chance < 94)
                        {
                            this.EnemyInit(EnemyType.Ghost);
                        }
                        else
                        {
                            this.EnemyInit(EnemyType.Quicker);
                        }
                    }
                }

                // Ha elfogyott az ellenség, győzelem
                if (this.enemies.Count == 0)
                {
                    ViewModel.Music = new SoundPlayer(Properties.Resources.WiningSound);
                    ViewModel.Music.Play();
                    MessageBoxResult result = MessageBox.Show("Szeretnél még egyet játszani?", "Gratulálunk, teljesítetted a pályát!", MessageBoxButton.YesNo);
                    Application.Current.Shutdown();
                    if (result == MessageBoxResult.Yes)
                    {
                        Process.Start("ArcherMan_V0CMMQ.exe");
                    }
                }
            }

            // Ellenségek mozgatása
            foreach (EnemyObject enemy in this.enemies)
            {
                enemy.Move(this.walls);
            }

            // Ha a játékos ütközik egy ellenséggel, vesztettél
            if (this.CollideWithEnemy(this.Enemies, this.player) != null)
            {
                ViewModel.Music = new SoundPlayer(Properties.Resources.SadViolin);
                ViewModel.Music.Play();
                MessageBoxResult result = MessageBox.Show("Új játék?!", "Vesztettél", MessageBoxButton.YesNo);
                Application.Current.Shutdown();
                if (result == MessageBoxResult.Yes)
                {
                    Process.Start("ArcherMan_V0CMMQ.exe");
                }
            }

            this.InvalidateVisual();
        }

        /// <summary>
        /// Draws the right objects on render.
        /// </summary>
        /// <param name="drawingContext"><see cref="DrawingContext"/></param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            foreach (Wall actWall in this.walls)
            {
                if (actWall != null)
                {
                    drawingContext.DrawImage(actWall.Sprite, actWall.GetGeometry.Bounds);
                }
            }

            if (this.Player != null)
            {
                drawingContext.DrawImage(this.Player.Sprite, this.Player.GetGeometry.Bounds);
            }

            foreach (Portal p in this.Portals)
            {
                drawingContext.DrawImage(p.Sprite, p.GetGeometry.Bounds);
            }

            foreach (EnemyObject enemy in this.Enemies)
            {
                drawingContext.DrawImage(enemy.Sprite, enemy.GetGeometry.Bounds);
            }

            if (this.ActualArrow != null)
            {
                drawingContext.PushTransform(this.ActualArrow.GetRotation);
            }

            if (this.ActualArrow != null)
            {
                drawingContext.DrawImage(this.ActualArrow.Sprite, this.ActualArrow.GetGeometry.Bounds);
            }
        }

        private void InitAll() // Kezdeti objektumok inicializálása
        {
            this.WallsInit();
            this.PlayerInit();
            this.ArrowInit();
            for (int i = 0; i < PORTALCOUNTER; i++)
            {
                this.PortalInit();
            }

            this.EnemyInit(EnemyType.Coin);
        }

        private void WallsInit() // Falak léterhozása
        {
            /*Azért adok hozzá vagy vonok ki egyet néhán helyen hogy az int és double közötti konverzió következtében esetleges réseket kitöltsem.*/
            double aroundWidth = this.Bounds.Width / Wall.VERTICALLY; // körbvevő fal szélessége
            double aroundHeight = this.Bounds.Height / Wall.HORIZONTALLY; // körbvevő fal magassága

            /*Körbevevő falak*/
            this.walls.AddLast(new Wall(this.Bounds.Width, aroundHeight, 0, 0, this)); // felső
            this.walls.AddLast(new Wall(aroundWidth, this.Bounds.Height, 0, 0, this)); // bal
            this.walls.AddLast(new Wall(aroundWidth, this.Bounds.Height, aroundWidth * (Wall.VERTICALLY - 1), 0, this)); // jobb
            this.walls.AddLast(new Wall(this.Bounds.Width, aroundHeight, 0, aroundHeight * (Wall.HORIZONTALLY - 1), this)); // alsó

            // Bal középső fal
            this.Walls.AddLast(new Wall(
                this.Bounds.Width / 2,
                this.Bounds.Height * 3 / Wall.HORIZONTALLY,
                aroundWidth - 1,
                (this.Bounds.Height / 2) - aroundHeight,
                this));

            ////Jobb alsó fal
            double actWallWidth = this.Bounds.Width * 17 / Wall.VERTICALLY;
            double actWallHeight = this.Bounds.Height * 8 / Wall.HORIZONTALLY;
            this.Walls.AddLast(new Wall(
                actWallWidth,
                actWallHeight,
                this.Bounds.Width - actWallWidth - aroundWidth + 1,
                this.Bounds.Height - actWallHeight - aroundHeight + 1,
                this));
        }

        private void PlayerInit() // Játékos léterhozása
        {
            Point start = new Point(
                    Wall.VERTICALLY + 1,
                     this.ActualHeight - Wall.HORIZONTALLY - 10);
            this.player = new Player(this, start);

            while (GameObject.CollideWithWall(this.Walls, this.Player))
            {
                start = new Point(start.X, start.Y - 5);
                this.player = new Player(this, start);
            }
        }

        private void PortalInit() // Megadott számú portál létrehozása random helyen
        {
            Point start;
            Portal p;
            do
            {
                start = new Point(rnd.Next(0, (int)this.ActualWidth), rnd.Next(0, (int)this.ActualHeight));
                p = new Portal(this, start);
            }
            while (GameObject.CollideWithWall(this.Walls, p));

            this.Portals.AddLast(p);
        }

        private void EnemyInit(EnemyType type) // Egy megadott típusú ellenség létrehozása
        {
            int index = rnd.Next(0, this.portals.Count);
            Portal p = this.Portals.ElementAt(index);
            switch (type)
            {
                default:
                case EnemyType.Coin: this.tempEnemy = new EvilCoin(this, p, this.player); break;
                case EnemyType.Ghost: this.tempEnemy = new Ghost(this, p, this.player); break;
                case EnemyType.Quicker: this.tempEnemy = new Quicker(this, p, this.player); break;
            }

            this.Enemies.AddLast(this.tempEnemy);
        }

        private void ArrowInit() // Nyíl létrehozása
        {
            switch (ViewModel.SelectedArrowType)
            {
                default:
                case ArrowType.Normal:
                    this.actualArrow = new MouseDirArrow(this.player, this);
                    break;
                case ArrowType.Following:
                    this.actualArrow = new PointerFollowingArrow(this.player, this);
                    break;
            }

            this.actualArrow.Visibility = true;
            this.Player.UsedArrow = this.ActualArrow;
        }

        private EnemyObject CollideWithEnemy(LinkedList<EnemyObject> enemies, GameObject obj) // Ellenséggel való ütközés
        {
            foreach (EnemyObject actEnemy in enemies)
            {
                if (GameObject.Collision(obj, actEnemy))
                {
                    return actEnemy;
                }
            }

            return null;
        }
    }
}
